package main

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"net/http"
	"strconv"
)

func parseFloatUtil(query string) (float64, error) {
	// Query is an empty string by default
	if query == "" {
		return 0, nil
	}

	queryFloat, err := strconv.ParseFloat(query, 64)
	if err != nil {
		return 0, err
	}

	return queryFloat, nil
}

func healthHandler(c *gin.Context) {
	c.Status(http.StatusOK)
}

func instrumentsListHandler(c *gin.Context, db *gorm.DB) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	pageSize := 100

	symbol := c.Query("symbol")
	name := c.Query("name")
	isin := c.Query("isin")
	instrumentType := c.Query("type")
	currency := c.Query("currency")
	maxPositionSize := c.Query("maxPositionSize")
	minTrade := c.Query("minTrade")

	maxPositionSizeFloat, err := parseFloatUtil(maxPositionSize)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error while parsing maxPositionSize"})
		log.Printf("Error while parsing maxPositionSizeFloat: %v", err)
	}

	minTradeFloat, err := parseFloatUtil(minTrade)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error while parsing minTrade"})
		log.Printf("Error while parsing minTradeFloat: %v", err)
	}

	conditions := Instrument{
		Symbol:          symbol,
		Name:            name,
		ISIN:            isin,
		Type:            instrumentType,
		Currency:        currency,
		MaxPositionSize: maxPositionSizeFloat,
		MinTrade:        minTradeFloat,
	}

	var instruments []Instrument

	if err := db.Offset((page - 1) * pageSize).Limit(pageSize).Where(conditions).Find(&instruments).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch instruments"})
		log.Printf("Failed to fetch instruments: %v", err)
		return
	}

	response := gin.H{
		"data":       instruments,
		"page":       page,
		"pageSize":   pageSize,
		"totalCount": totalInstrumentCount,
	}

	c.JSON(http.StatusOK, response)
}

func instrumentsIdHandler(c *gin.Context, db *gorm.DB) {
	id := c.Param("id")
	_, err := strconv.Atoi(id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "The id should be an integer"})
		log.Printf("instrumentsIdHandler bad request: %v", err)
		return
	}

	var instrument Instrument
	if err := db.First(&instrument, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintf("instrument with id %v does not exist", id)})
			log.Printf("instrumentsIdHandler bad request: %v", err)
			return
		}

		c.JSON(http.StatusBadRequest, gin.H{"error": "something went wrong"})
		log.Printf("instrumentsIdHandler bad request: %v", err)
		return
	}

	c.JSON(http.StatusOK, instrument)
}
