package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
	"gorm.io/gorm"
	"log"
	"time"
)

func setupRouter() *gin.Engine {
	router := gin.Default()

	err := router.SetTrustedProxies(nil)
	if err != nil {
		log.Fatal(err)
	}

	return router
}

func setupCron(db *gorm.DB) {
	s := gocron.NewScheduler(time.UTC)

	// Function also runs on init
	_, err := s.Every(24).Hours().Do(func() {
		ScrapeInstruments(db)
	})

	if err != nil {
		log.Fatal(err)
	}

	s.StartAsync()
}

func main() {
	db := setupDatabase()
	setupCron(db)
	router := setupRouter()

	router.GET("/health", func(c *gin.Context) {
		healthHandler(c)
	})

	router.GET("/v1/instruments", func(c *gin.Context) {
		instrumentsListHandler(c, db)
	})

	router.GET("/v1/instruments/:id", func(c *gin.Context) {
		instrumentsIdHandler(c, db)
	})

	err := router.Run()
	if err != nil {
		log.Fatal(err)
	} // listen and serve on 0.0.0.0:8080
}
