package main

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	totalInstrumentCount int64
)

type Instrument struct {
	ID              uint    `gorm:"primaryKey" json:"id"`
	Symbol          string  `gorm:"index" json:"symbol"`
	Name            string  `gorm:"index" json:"name"`
	ISIN            string  `gorm:"index" json:"isin"`
	Type            string  `gorm:"index" json:"type"`
	Currency        string  `gorm:"index" json:"currency"`
	MaxPositionSize float64 `gorm:"index" json:"maxPositionSize"`
	MinTrade        float64 `gorm:"index" json:"minTrade"`
}

func setupDatabase() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{
		PrepareStmt:            true,
		SkipDefaultTransaction: true,
	})
	if err != nil {
		panic(fmt.Sprintf("Failed to connect database: %v", err))
	}

	err = db.AutoMigrate(&Instrument{})
	if err != nil {
		panic(fmt.Sprintf("Failed to migrate database schema: %v", err))
	}

	return db
}

func resetDatabase(db *gorm.DB) {
	sqlDB, err := db.DB()
	if err != nil {
		panic(fmt.Sprintf("Failed to retrieve underlying database: %v", err))
	}

	err = sqlDB.Close()
	if err != nil {
		panic(fmt.Sprintf("Failed to close database: %v", err))
	}

	newDb := setupDatabase()

	*db = *newDb
}

func writeInstruments(instruments []Instrument, db *gorm.DB) {
	// 2000 Seems to be around the limit for max sql variables
	result := db.CreateInBatches(&instruments, 2000)

	if result.Error != nil {
		panic(fmt.Sprintf("Failed to write instruments to the database: %v", result.Error))
	}
}

func updateTotalInstrumentCount(db *gorm.DB) {
	if err := db.Table("instruments").Count(&totalInstrumentCount).Error; err != nil {
		panic(fmt.Sprintf("Failed to count instruments: %v", err))
	}
}
