package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"gorm.io/gorm"
	"log"
	"net/http"
	"strings"
)

func scrapeData() ([]interface{}, error) {
	url := "https://www.trading212.com/trading-instruments/invest"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func() {
		if bodyErr := resp.Body.Close(); bodyErr != nil {
			log.Fatal(bodyErr)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP request failed with status: %s", resp.Status)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}

	script := doc.Find("script[id=__NEXT_DATA__]").First().Text()
	script = strings.TrimSpace(script)

	var dataObj map[string]interface{}
	if err := json.Unmarshal([]byte(script), &dataObj); err != nil {
		return nil, err
	}

	items := dataObj["props"].(map[string]interface{})["pageProps"].(map[string]interface{})["instruments"].(map[string]interface{})["items"].([]interface{})

	return items, nil
}

func transformData(rawInstruments []interface{}) []Instrument {
	resultInstruments := make([]Instrument, len(rawInstruments))

	for i, rawInstrument := range rawInstruments {
		instrumentMap := rawInstrument.(map[string]interface{})

		resultInstruments[i] = Instrument{
			Symbol:          instrumentMap["ticker"].(string),
			Name:            instrumentMap["fullName"].(string),
			ISIN:            instrumentMap["isin"].(string),
			Type:            instrumentMap["type"].(string),
			Currency:        instrumentMap["currency"].(string),
			MaxPositionSize: instrumentMap["maxOpenLong"].(float64),
			MinTrade:        instrumentMap["minTrade"].(float64),
		}
	}

	return resultInstruments
}

func ScrapeInstruments(db *gorm.DB) {
	data, err := scrapeData()
	if err != nil {
		log.Fatal(err)
	}

	instruments := transformData(data)
	resetDatabase(db)
	writeInstruments(instruments, db)
	updateTotalInstrumentCount(db)
}
