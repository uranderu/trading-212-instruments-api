FROM golang:latest AS build

WORKDIR /app
COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o app ./cmd

FROM debian:latest

RUN apt-get update && apt-get install -y ca-certificates # Necessary for scraper

ENV GIN_MODE="release"

WORKDIR /app
COPY --from=build /app/app .

EXPOSE 8080

CMD ["./app"]
